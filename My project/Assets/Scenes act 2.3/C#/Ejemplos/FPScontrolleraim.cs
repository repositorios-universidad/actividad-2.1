using UnityEngine;

public class PlayerFPSController : MonoBehaviour
{
    public GameObject cam;
    public float walkSpeed = 5f;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;
    // inicialization
    void Start()
    {
        //visibilidad del mouse
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("Capsule").gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        movement();

        // rotacion
        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;


        transform.Rotate(0f, hPlayerRotation, 0f);
        cam.transform.Rotate(-vCamRotation, 0f, 0f);
    }


    private void movement()
    {

        //movimiento
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovenemt = Input.GetAxisRaw("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovenemt * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));


    }
}
